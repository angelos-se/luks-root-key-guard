#!/bin/sh

# VARS: Script internal
LSBLK="lsblk -l -o KNAME,PKNAME,FSTYPE,UUID,MOUNTPOINT"

# VARS: Installer options
install_opts="--owner=root --group=root -Z"

# VARS: Program defauls
pkey="/etc/luks-root-key-guard.pkey"
tkey="/etc/machine-id"


# Copy files
install $install_opts --mode=0744 \
  --target-directory=/usr/local/sbin/ \
  luks-root-key-guard-01start.sh \
  luks-root-key-guard-99stop.sh
install $install_opts --mode=0644 \
  --target-directory=/usr/lib/systemd/system \
  luks-root-key-guard.service

# Create new permanent key
if ! [ -s $pkey ]; then
  umasksaved="$(umask -p)"
  umask 0077
  dd if=/dev/urandom of=$pkey bs=512 count=1
  umask $umasksaved
fi

# Find LUKS device of root volume
PKNAME="$($LSBLK | grep /$ | awk '{print $2}')"
while [ "$FSTYPE" != "crypto_LUKS" ]; do
  lsblkrec="$($LSBLK | grep ^$PKNAME)"
  KNAME="$(echo $lsblkrec | awk '{print $1}')"
  PKNAME="$(echo $lsblkrec | awk '{print $2}')"
  FSTYPE="$(echo $lsblkrec | awk '{print $3}')"
  UUID="$(echo $lsblkrec | awk '{print $4}')"
done
echo "INFO: Found rootfs on LUKS device $KNAME with $UUID"

# Backup and modify crypttab
crypttabbackup="crypttab.$(date +%s)"
cp -a /etc/crypttab /etc/$crypttabbackup
sed -i /$UUID/s:none:$tkey: /etc/crypttab

# Display modifications and rebuild initramfs
diff /etc/$crypttabbackup /etc/crypttab
rc=$?
if [ "$rc" -eq 0 ]; then
  echo "WARNING: sed made no change to crypttab, please manually verify"
else
  dracut -f
fi

# Install pkey
if ! cryptsetup luksAddKey /dev/$KNAME $pkey; then
  echo "ERROR: cryptsetup luksAddKey exited with error"
fi

# Write out config
echo pkey="/etc/luks-root-key-guard.pkey" >> /etc/sysconfig/luks-root-key-guard.conf
echo tkey="/etc/machine-id" >> /etc/sysconfig/luks-root-key-guard.conf
echo luksDisk="/dev/$KNAME" >> /etc/sysconfig/luks-root-key-guard.conf

# Enable service
systemctl daemon-reload
systemctl enable --now luks-root-key-guard.service
systemctl status luks-root-key-guard.service
