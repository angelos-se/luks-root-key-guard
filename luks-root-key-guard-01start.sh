#!/bin/sh

if ! ( [ -b "$luksDisk" ] && [ -f "$pkey" ] && [ -f "$tkey" ] ) ; then
	echo "One or more bad options: luksDisk=$luksDisk pkey=$pkey tkey=$tkey"
	exit 1
fi

luksUUID="$(blkid | grep $luksDisk | cut -d\" -f2)"
if ! grep "$luksUUID" /etc/crypttab | awk '{print $3}' | grep -q "$tkey"; then
	echo "Bad crypttab"
	exit 2
fi

initramfsimgs="$(ls -rt /boot/initramfs-*.img | egrep -v 'rescue|kdump')"
for i in $initramfsimgs; do
	if ! lsinitrd -f etc/crypttab $i | grep -q -E [[:space:]]$tkey[[:space:]]? ; then
		kver="$(echo $i | sed s:/boot/initramfs-::g | sed s:.img::g)"
		dracut -f $kver
	fi
done

if cryptsetup luksDump "$luksDisk"; then
	while cryptsetup luksRemoveKey "$luksDisk" "$tkey"; do
		cryptsetup luksDump "$luksDisk"
	done
else
	echo "cryptsetup luksDump encountered error"
	exit 4
fi
