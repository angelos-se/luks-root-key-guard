#!/bin/sh

if ! ( [ -b "$luksDisk" ] && [ -f "$pkey" ] && [ -f "$tkey" ] ) ; then
	echo "One or more bad options: luksDisk=$luksDisk pkey=$pkey tkey=$tkey"
	exit 1
fi

initramfsimgs="$(ls -rt /boot/initramfs-*.img | egrep -v 'rescue|kdump')"
for i in $initramfsimgs; do
	if ! lsinitrd -f etc/crypttab $i | grep -q -E [[:space:]]$tkey[[:space:]]? ; then
		kver="$(echo $i | sed s:/boot/initramfs-::g | sed s:.img::g)"
		dracut -f $kver
	fi
done

if cryptsetup luksDump "$luksDisk"; then
	if cryptsetup --key-file="$pkey" luksAddKey "$luksDisk" "$tkey"; then
		cryptsetup luksDump "$luksDisk"
	else
		echo "cryptsetup luksDump encountered error"
		exit 4
	fi
fi
